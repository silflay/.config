if !exists('g:lspconfig')
  finish
endif


lua << EOF
local nvim_lsp = require('lspconfig')

-- Use an on_attach function to only map the following keys 
-- after the language server attaches to the current buffer
local on_attach = function(client, bufnr)
  local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end

  -- Mappings.
  local opts = { noremap=true, silent=true }

  -- See `:help vim.lsp.*` for documentation on any of the below functions
  buf_set_keymap('n', 'gD', '<Cmd>lua vim.lsp.buf.declaration()<CR>', opts)
  buf_set_keymap('n', 'gd', '<Cmd>lua vim.lsp.buf.definition()<CR>', opts)
end

vim.diagnostic.config({
  virtual_text = false,
})

-- backend

nvim_lsp.csharp_ls.setup {
  on_attach = on_attach,
  filetypes = { 'cs' },
}

-- didn't get this to work, using intelephense instead
-- nvim_lsp.phpactor.setup {
--   on_attach = on_attach,
--   filetypes = { 'php', "twig" },
--   init_options = {
--     ['language_server_phpstan.enabled'] = false,
--     ['language_server_psalm.enabled'] = false,
--   },
-- }

nvim_lsp.intelephense.setup {
  on_attach = on_attach,
  filetypes = {
    'php',
    'twig',
  },
}


-- frontend
nvim_lsp.tailwindcss.setup {
  on_attach = on_attach,
  filetypes = {
    'html',
    'markdown',
    'php',
    'razor',
    'twig',
    'css',
    'less',
    'postcss',
    'sass',
    'scss',
    'typescript',
    'javascript',
    'javascriptreact',
    'vue',
    'svelte',
  },
}

nvim_lsp.ts_ls.setup {
  on_attach = on_attach,
  filetypes = {
    'typescript',
    'javascript',
  },
}

nvim_lsp.volar.setup {
  on_attach = on_attach,
  filetypes = {
    'typescript',
    'javascript',
    'javascriptreact',
    'typescriptreact',
    'vue',
    'json',
  },
}

nvim_lsp.eslint.setup({
  -- manual set of rules dependant on VS Code nodeEnv = 'development'
  -- https://github.com/microsoft/vscode-eslint/blob/55871979d7af184bf09af491b6ea35ebd56822cf/server/src/eslintServer.ts#L216-L229
  settings = {
    rulesCustomizations = {
      {
        rule = 'no-console',
        severity = 'off',
      },
    }
  },

  on_attach = function(client, bufnr)
    vim.api.nvim_create_autocmd('BufWritePre', {
      buffer = bufnr,
      command = 'EslintFixAll',
    })
  end,
})

-- nvim_lsp.vuels.setup {
--   on_attach = on_attach,
-- }

EOF
