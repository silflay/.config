if !exists('g:loaded_telescope') | finish | endif

" find file by name
nnoremap  <silent> ;f <cmd>lua require('telescope.builtin').find_files()<cr>
nnoremap  <silent> ;b <cmd>lua require('telescope.builtin').file_browser()<cr>
nnoremap <silent> ;; <cmd>Telescope buffers<cr>

" find file by code
nnoremap  <silent> ;l <cmd>lua require('telescope.builtin').lsp_references()<cr>
nnoremap  <silent> ;w <cmd>lua require('telescope.builtin').grep_string()<cr>
nnoremap  <silent> ;g <cmd>lua require('telescope.builtin').live_grep()<cr>
nnoremap  <silent> ;r <cmd>Telescope resume<cr>
nnoremap  <silent> ;p <cmd>Telescope pickers<cr>

" help
nnoremap <silent> ;h <cmd>Telescope help_tags<cr>

nnoremap <silent> ;s <cmd>Telescope live_grep default_text=<C-R>"<cr>

lua << EOF
function telescope_buffer_dir()
  return vim.fn.expand('%:p:h')
end

local telescope = require('telescope').setup{
  defaults = {
    file_ignore_patterns = {
      '%.min.js',
      '%.min.css',
      '%.min.css.map',
      '%.css.map',
      '%.lock',
      '%.png',
      '%.gif',
      '%.jpg',
      '%.ttf',
      '%.eot',
      '%.woff',
      '%.woff2',

      '**/scripts/*',
      '**/styles/css/main.css',
      '**/leaflet/leaflet.js',
    },
  },
};

local actions = require('telescope.actions')
EOF
