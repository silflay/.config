require('dashboard').setup{
  theme = 'hyper',
  config = {
    week_header = {
      enable = true,  --boolean use a week header
    },
    shortcut = {
      { desc = ' Update', group = '@property', action = 'Lazy update', key = 'u' },
      {
        desc = ' Files',
        group = 'Label',
        action = 'Telescope find_files',
        key = 'f',
      },
      {
        desc = ' Apps',
        group = 'DiagnosticHint',
        action = 'Telescope app',
        key = 'a',
      },
      {
        desc = ' dotfiles',
        group = 'Number',
        action = 'Telescope dotfiles',
        key = 'd',
      },
    },
    project = { enable = true, limit = 8, icon = '  ', label = 'Projects', action = 'Telescope find_files cwd=' },
  }
}
