local green = '#50fa7b';
local pink = '#ff79c6';
local bg = '#282a36';
local purple = '#bd93f9';
local white = '#ffffff';
local black = '#000000';
local gray = '#44475a';
local yellow = '#f1fa8c';

require("incline").setup({
				highlight = {
					groups = {
						InclineNormal = { guifg = white },
						InclineNormalNC = { guifg = purple },
					},
				},
				window = { margin = { vertical = 0, horizontal = 0 } },
				hide = {
					cursorline = true,
				},
				render = function(props)
					local filename = vim.fn.fnamemodify(vim.api.nvim_buf_get_name(props.buf), ":t")
					if vim.bo[props.buf].modified then
						filename = "[+] " .. filename
					end

					local icon, color = require("nvim-web-devicons").get_icon_color(filename)
					return { { icon, guifg = color }, { " " }, { filename } }
				end,
			})
