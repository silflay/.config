" /.local/share/nvim on mac
let g:plug_home = stdpath('data') . '/plugged'

" Start
call plug#begin()

" Theme
Plug 'dracula/vim', { 'as': 'dracula' }
Plug 'hoob3rt/lualine.nvim'
Plug 'kyazdani42/nvim-web-devicons'
Plug 'echasnovski/mini.animate'
Plug 'b0o/incline.nvim'
Plug 'nvimdev/dashboard-nvim'

Plug 'folke/noice.nvim'
Plug 'MunifTanjim/nui.nvim' " Dependancy
Plug 'rcarriga/nvim-notify', " Dependancy

Plug 'rachartier/tiny-inline-diagnostic.nvim'
Plug 'shellRaining/hlchunk.nvim'

" LSP
Plug 'neovim/nvim-lspconfig'
Plug 'glepnir/lspsaga.nvim'

" Syntax
Plug 'leafgarland/typescript-vim'
Plug 'nvim-treesitter/nvim-treesitter', { 'do': ':TSUpdate' }
Plug 'osyo-manga/vim-brightest'
Plug 'nelsyeung/twig.vim'
Plug 'uga-rosa/ccc.nvim'

" Movement
Plug 'easymotion/vim-easymotion'

" Plug 'tomtom/tcomment_vim'
Plug 'numToStr/Comment.nvim'

Plug 'nvim-lua/popup.nvim'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'

" Plug 'ctrlpvim/ctrlp.vim'
" Plug 'gcmt/taboo.vim'

" Typing helpers
Plug 'onsails/lspkind-nvim'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/nvim-cmp'
Plug 'tpope/vim-surround'
Plug 'L3MON4D3/LuaSnip'

" Navigation
Plug 'kristijanhusak/defx-icons'
Plug 'Shougo/defx.nvim', { 'do': ':UpdateRemotePlugins' }

" Git
Plug 'tpope/vim-fugitive'
Plug 'kristijanhusak/defx-git'

" Session
" Plug 'rmagatti/auto-session'

" Buffers
Plug 'akinsho/bufferline.nvim'

" AI
" Plug 'jackMort/ChatGPT.nvim'
" Plug 'MunifTanjim/nui.nvim' " Dependancy
" Plug 'nvim-lua/plenary.nvim' " Dependancy

" End
call plug#end()
