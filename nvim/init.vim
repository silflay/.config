" " Imports "{{{
" ---------------------------------------------------------------------
runtime ./plug.vim


" " General "{{{
" ---------------------------------------------------------------------
" Encoding
set enc=utf-8
set nobomb

"  leader
let mapleader = ','

" mis
set noswapfile
set nofixendofline
set nobackup
set splitright
set number
set signcolumn=yes
set autoindent

" use 2 spaces for indentation
set expandtab
set shiftwidth=2 

" show existing tab with 2 spaces
set tabstop=2
set softtabstop=2

" Quick save&exit/exit
nnoremap <leader>w :w<CR>
nnoremap <leader>x :x<CR>
nnoremap <leader>q :q<CR>

" Quick highlight clear
nnoremap <Leader><space> :noh<cr>


" " Navigation "{{{
" ---------------------------------------------------------------------
" quick tab creation
nnoremap <C-n> :tab new<CR>

" explorer shortcut
map <leader>ne :Explore<CR>

" ~/nvim shortcut
map <leader>init :vsp ~/nvim/init.vim<CR>
map <leader>plug :vsp ~/nvim/plug.vim<CR>

" scroll offset
" set so=999
nnoremap j jzz
nnoremap k kzz
nnoremap H Hzz
nnoremap L Lzz
nnoremap <C-f> <C-f>zz
nnoremap <C-b> <C-b>zz
nnoremap { {zz
nnoremap } }zz
nnoremap ( (zz
nnoremap ) )zz
nnoremap <C>o <C>ozz

" Highlights "{{{
" ---------------------------------------------------------------------
set cursorline


" Theme "{{{
" ---------------------------------------------------------------------
syntax enable
set termguicolors
colorscheme dracula

" Transparent BG
hi Normal guibg=NONE ctermbg=NONE
" hi Normal ctermfg=250 ctermbg=none
" hi NonText ctermfg=250 ctermbg=none
" hi CursorLine ctermfg=260 ctermbg=233
" hi Visual ctermfg=243 ctermbg=233
" hi StatusLine ctermbg=white ctermfg=black
" highlight LineNr ctermfg=gray ctermbg=none


" Easy Motion "{{{
" ---------------------------------------------------------------------
" One line find
nmap f <Plug>(easymotion-fl)
nmap F <Plug>(easymotion-Fl)

" Document find
map / <Plug>(easymotion-sn)
omap / <Plug>(easymotion-tn)

" General settings
let g:EasyMotion_smartcase = 1
let g:EasyMotion_use_upper = 1
let g:EasyMotion_keys = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ;'


" Fugitive "{{{
" ---------------------------------------------------------------------
nnoremap <leader>gs :vertical G<CR>
nnoremap <leader>gc :vertical Git commit<CR>
nnoremap <leader>gl :vertical Git log<CR>
nnoremap <leader>gd :vertical Git diff<CR>
nnoremap <leader>gh :vertical Git show<CR>
nnoremap <leader>ga :vertical Git commit --amend<CR>
nnoremap <leader>gca :Git commit --amend --no-edit<CR>
nnoremap <leader>gb :vertical Git branch<CR>
nnoremap <leader>gbu :Git branch -u origin/
nnoremap <leader>gbd :Git branch -D<Space>
nnoremap <leader>go :Git checkout<Space>
nnoremap <leader>gob :Git checkout -b<Space>
nnoremap <leader>gr :Git rebase<Space>
nnoremap <leader>gm :Git merge<Space>
nnoremap <leader>gpull :Git pull<CR>
nnoremap <leader>gpushu :Git push --set-upstream origin<Space>


" Ctrl+P "{{{
" " ---------------------------------------------------------------------
" set wildignore+=*/tmp/*,*.so,*.swp,*.zip
" let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files -co --exclude-standard']
" let g:ctrlp_working_path_mode = 'r'

" special rebuild & cache clear
function! Cmd()
  let start_time = reltime()
  " execute 'silent !npm --prefix /Users/xlinty/www/researchdata.se/cms/src/web/modules/custom/ckeditor_snd run build && docker exec researchdata_cms drush cr'
  execute 'silent !npm --prefix /home/tobias/www/researchdata.se/cms/src/web/modules/custom/ckeditor_snd run build && docker exec researchdata_cms drush cr'
  redraw!
  echo 'rebuilt & cleared cache in' reltimestr(reltime(start_time)) 'seconds'
endfunction

nnoremap <leader>b :w<CR>:call Cmd()<CR>

" if system('uname -r') =~ "Microsoft"
if system('uname -r') =~ "Microsoft"
  augroup Yank
    autocmd!
    autocmd TextYankPost * :call system('/mnt/c/windows/system32/clip.exe ',@")
  augroup END
endif
