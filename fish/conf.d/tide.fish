set -g tide_os_bg_color 5f5f87
set -g tide_os_color ffffff

set -g tide_time_bg_color af87ff
set -g tide_time_color 000000

set -g tide_node_bg_color 44475a
set -g tide_node_color 50fa7b
set -g tide_node_icon 

set -g tide_vi_mode_bg_color_default 50fa7b
set -g tide_vi_mode_color_default 000000
set -g tide_vi_mode_bg_color_insert af87ff
set -g tide_vi_mode_color_insert 000000
set -g tide_vi_mode_bg_color_replace ff5555
set -g tide_vi_mode_color_replace 000000
set -g tide_vi_mode_bg_color_visual f1fa8c
set -g tide_vi_mode_color_visual 000000

set -g tide_kubectl_bg_color 5f5f87
set -g tide_kubectl_color ffffff
set -g tide_kubectl_icon ■

set -g tide_php_bg_color 44475a
set -g tide_php_color 50fa7b
set -g tide_php_icon 

set -g tide_cmd_duration_bg_color 44475a
set -g tide_cmd_duration_color f1fa8c
set -g tide_cmd_duration_decimals 0
set -g tide_cmd_duration_icon 
set -g tide_cmd_duration_threshold 3000

set -g tide_status_bg_color 44475a
set -g tide_status_bg_color_failure 44475a
set -g tide_status_color 50fa7b
set -g tide_status_color_failure ff5555
set -g tide_status_icon ✓
set -g tide_status_icon_failure ✘

set -g tide_pwd_bg_color 5f5f87
set -g tide_pwd_color ffffff
set -g tide_pwd_color_anchors ffffff
set -g tide_pwd_color_color_anchors ffffff
set -g tide_pwd_color_dirs ffffff
set -g tide_pwd_color_truncated_dirs ffffff

set -g tide_git_bg_color 44475a
set -g tide_git_bg_color_unstable 44475a
set -g tide_git_bg_color_urgent 44475a

set -g tide_git_branch_color ffb86c
set -g tide_git_color_branch ffb86c

set -g tide_git_color_conflicted 44475a
set -g tide_git_conflicted_color 44475a

set -g tide_git_color_stash ff79c6
set -g tide_git_stash_color ff79c6

set -g tide_git_color_staged 50fa7b
set -g tide_git_staged_color ffffff

set -g tide_git_color_operation ffffff
set -g tide_git_operation_color ffffff

set -g tide_git_untracked_color 44475a
set -g tide_git_color_untracked 44475a

set -g tide_git_color_upstream f1fa8c
set -g tide_git_upstream_color f1fa8c

set -g tide_git_color_dirty ff5555
set -g tide_git_dirty_color ff5555

set -g tide_git_icon 
